<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-checkbox</div>
<div class="c-checkbox">
	<input type="checkbox" name="a" id="a" />
	<label for="a" class="c-checkbox__text">オプション</label>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-tab</div>
<div class="c-tab js_tab">
	<ul class="c-tab__wrap" >
		<li class="c-tab__link" data-tab="tab-1">
			<div class="c-tab__img">
				<img src="/assets/img/common/icon/icon_net-research.png" alt="" width="30" height="30">
			</div>
			<h3 class="c-tab__ttl">ネット<br>リサーチ</h3>
		</li>
		<li class="c-tab__link" data-tab="tab-2">
			<div class="c-tab__img">
				<img src="/assets/img/common/icon/icon_pdca.png" alt="" width="30" height="30">
			</div>
			<h3 class="c-tab__ttl">プロセス<br>から探す</h3>
		</li>
		<li class="c-tab__link" data-tab="tab-3">
			<div class="c-tab__img">
				<img src="/assets/img/common/icon/icon_questionnaire.png" alt="" width="28" height="30">
			</div>
			<h3 class="c-tab__ttl">アンケート<br>調査から探す</h3>
		</li>
		<li class="c-tab__link current" data-tab="tab-4">
			<div class="c-tab__img">
				<img src="/assets/img/common/icon/icon_solution.png" alt="" width="30" height="30">
			</div>
			<h3 class="c-tab__ttl">課題から<br>探す</h3>
		</li>
		<li class="c-tab__link" data-tab="tab-5">
			<div class="c-tab__img">
				<img src="/assets/img/common/icon/icon_statistics.png" alt="" width="30" height="30">
			</div>
			<h3 class="c-tab__ttl">統計・分析<br>手法から探す</h3>
		</li>
	</ul>

	<div class="c-tab__content tab-1">
		Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</div>
	<div class="c-tab__content tab-2">
		Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</div>
	<div class="c-tab__content tab-3">
		Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</div>
	<div class="c-tab__content tab-4 current">
		<div class="c-tab__inner">
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a1" id="a1" />
					<label for="a1" class="c-checkbox__text">商品・サービス開発</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a2" id="a2" />
					<label for="a2" class="c-checkbox__text">市場・生活者ニーズ把握</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a3" id="a3" />
					<label for="a3" class="c-checkbox__text">広告・コミュニケーション戦略</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a4" id="a4" />
					<label for="a4" class="c-checkbox__text">ブランディング</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a5" id="a5" />
					<label for="a5" class="c-checkbox__text">店舗・施設設計</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a6" id="a6" />
					<label for="a6" class="c-checkbox__text">エリアマーケティング</label>
				</div>
			</div>
			<div class="c-tab__item">
				<div class="c-checkbox">
					<input type="checkbox" name="a7" id="a7" />
					<label for="a7" class="c-checkbox__text">オリジナルメソッド</label>
				</div>
			</div>
		</div>
	</div>
	<div class="c-tab__content tab-5">
		Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt .
	</div>
</div>