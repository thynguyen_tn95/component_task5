<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<ul class="c-list1">
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">セルフ型アンケートツール</p>
				<p class="c-list1__text">QiQUMO</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/101.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">消費者の生活や感情に密着できる</p>
				<p class="c-list1__text">オフラインリサーチ</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">目的や用途、手法に合わせた</p>
				<p class="c-list1__text">データ分析・解析</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">学術研究機関の研究者や学生向け</p>
				<p class="c-list1__text">学術調査</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">無料集計ツール</p>
				<p class="c-list1__text">Cross Finder</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">リサーチにおける重要性</p>
				<p class="c-list1__text">パネルについて</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">20歳から26歳男子の会話から<br>ホンネが見えるコミュニティ</p>
				<p class="c-list1__text">U26</p>
			</div>
		</a>
	</li>
	<li class="c-list1__card">
		<a href="" class="c-list1__inner">
			<div class="c-list1__img">
				<img src="/assets/img/common/100.png" alt="" width="160" height="160">
			</div>
			<div class="c-list1__info">
				<p class="c-list1__ttl">シニアのホンネに会える<br>Face to Faceコミュニティー</p>
				<p class="c-list1__text">iDOBATA KAIGI</p>
			</div>
		</a>
	</li>
</ul>