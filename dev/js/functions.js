/* ======================================
tab
====================================== */
$(document).ready(function(){
	$('.c-tab__link').click(function() {
		var content = $('.c-tab__link').index(this);
		$('.c-tab__link').removeClass('current');
		$(this).addClass('current');
		$('.c-tab__content').removeClass('current');
		$('.c-tab__content').eq(content).addClass('current');
	});
});
